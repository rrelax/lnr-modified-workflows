import requests
import arrow
import urllib.parse
import json
import os

# Inserted GitLab interface code

import gitlab

def create_new_issue(project, token, issue_title, issue_markdown_content):
    print("Creating issue...")

    issue_creation_client = gitlab.Gitlab(private_token=token) 
    current_project = issue_creation_client.projects.get("rrelax/lnr-modified-workflows")
    issue_details = {
        'title': f'{issue_title}',
        'description': f'{issue_markdown_content}',
    }
    return current_project.issues.create(issue_details )

# End new code

token = os.environ["auth"]
issue_creation_token = os.environ["issue_creation_token"]

gl = gitlab.Gitlab(private_token=token) # Used for creating the issue - could be consolidated to use the REST API
project = gl.projects.get("rrelax/lnr-modified-workflows")
branch = "main"
base_path = "https://gitlab.com/api/v4/projects/"
project_id_number = "42817607"

# LNR Workflow root: https://about.gitlab.com/handbook/support/license-and-renewals/workflows/

# This code is gross but it works! Desperately needs a refactor (2024-05-06 - this is even truer than when this line was first authored)

def _prepare_tree_url(base_path, project_id):
    prepared_string = f"{base_path}{project_id}/repository/tree/"
    return prepared_string


def get_api_url(project_namespace):
    project_namespace_encoded = urllib.parse.quote(project_namespace).replace("/", "%2F")
    prepared_string = f"https://gitlab.com/api/v4/projects/{project_namespace_encoded}/"
    return prepared_string


def get_commit_MRs(project_namespace, commit_id, token):
    print(project_namespace, " ", commit_id)
    prepared_url = get_api_url(project_namespace)+f"repository/commits/{commit_id}/merge_requests?status=merged"
    print(prepared_url)
    response = requests.get(prepared_url, headers={'PRIVATE-TOKEN': token})
    print()
    return response

def link_transformer(url):
    url = url.replace("sites/handbook/source/", "https://about.gitlab.com/")
    url = url.replace(".html.md", ".html")
    return url


def get_repo_file_record(project_namespace, file_path, token):
    file_path_encoded = urllib.parse.quote(file_path).replace("/", "%2F")
    prepared_url = get_api_url(project_namespace)+f"repository/files/{file_path_encoded}"
    response = requests.get(prepared_url, headers={'PRIVATE-TOKEN': token}, params={"ref":"main"})
    return response


def get_commit(project_namespace, commit_id, token):
    prepared_url = get_api_url(project_namespace)+f"repository/commits/{commit_id}"

    response = requests.get(prepared_url, headers={'PRIVATE-TOKEN': token})
    return response


def search_dir(tree_path, token, branch_name, path, responses = None):
    if responses == None:
        responses = []
    
    response = requests.get(
        tree_path,
        headers={'PRIVATE-TOKEN': token},
        params={"path": path, "ref": branch_name }
    )
    tree_payload = json.loads(response.text)
    responses.append(tree_payload)

    try:
        while response.links['next'] != None:
            url = response.links['next']['url']
            response = requests.get(
                url,
                headers={'PRIVATE-TOKEN': token},
            )
            tree_payload = json.loads(response.text)
            responses.append(tree_payload)
    except KeyError:
        pass
    return responses

# gitlab-com/content-sites/handbook/-/tree/main/content/handbook/support/license-and-renewals/workflows?ref_type=heads
project_id = "handbook"

tree_path = _prepare_tree_url(base_path, project_id_number)
test_dir_path = "content/handbook/support/license-and-renewals/workflows" # TODO: Remove magic variables

paramsObj ={"path": test_dir_path, "ref": branch }

response = requests.get( # TODO: determine whethere this is used
    tree_path,
    headers={'PRIVATE-TOKEN': token},
    params=paramsObj
) 

responses = search_dir(tree_path, token, branch, path="content/handbook/support/license-and-renewals/workflows/")  # TODO: Remove magic variables

sub_directories = []
files = []
print(f"Initial count of files to examine (including workflows and subdirs): {len(responses)}")
for payload in responses:
    if "error" in payload:
        print(payload)
        exit(1)

    for item in payload:
        try:
            if item["type"] == "tree":
                print(f"\tChecking subdirectory: {item['name']}")
                search_dir(tree_path, token, branch, path=item["path"], responses=responses)
            if item["type"] == "blob":
                if ".md" in item["path"]:
                    files.append(item)

        except TypeError as exc:
            print(exc)
            print()
            print(repr(exc))
            exit(1)

print(f"\n\n\tDirs: {len(sub_directories)}\n\tFiles: {len(files)}\n")

updated_files = []

namespace_unencoded = "gitlab-com/content-sites"
threshold_date = arrow.now().shift(days=-7)
print(f"Determining workflow updates since {threshold_date}...\n\n")


for file_result in files:
    file_obj_response = get_repo_file_record(project_id_number,file_result["path"],token)
    file_obj = json.loads(file_obj_response.text)
    try:
        commit_id = file_obj["last_commit_id"]
    except KeyError as exc:
        print("Key error!")
        print(exc)
        print(file_obj)
        exit(1)
    
    response = get_commit(project_id_number, commit_id, token)
    commit_metadata = json.loads(response.text)
    file_obj["message"] = commit_metadata['message']
    commit_date = arrow.get(commit_metadata['committed_date'])
    if commit_date > threshold_date:
        # Find parent MR
        related_commit_MRs = get_commit_MRs(project_id_number, commit_id, token)
        parsed_MR_references = json.loads(related_commit_MRs.text)

        merged_items = [item for item in parsed_MR_references if item.get('state') == 'merged']
        most_recent = max(merged_items, key=lambda x: x.get('merged_at', ''))
        file_obj["merge_id"] = most_recent["iid"]

        # Set commit_date
        file_obj["commit_date"] = commit_date
        updated_files.append(file_obj)


print("\n\n\nUPDATED FILES:")
summary = "Copy and paste the text below the heading to [the L&R sync Google doc](https://docs.google.com/document/d/1G59jOZzJkelHzLgussZYNZuzNJBD8qnivkPeOvZUrTQ/edit#)\n\n#### Features/bugs/MRs created recently [[script repo]](https://gitlab.com/rrelax/lnr-modified-workflows)  \n\n"

for update in updated_files:
    file_name = update['file_name']
    date = update["commit_date"]
    path = update["file_path"]
    humanized = arrow.get(date).humanize()
    url = link_transformer(path)
    change_summary_title = "\"_"+update["message"].replace("\n","")+"_\""

    merge_id = update["merge_id"]

    summary += f"- [{file_name}]({url})\n"
    summary += f"  - {change_summary_title}\n" # Summary text
    summary += f"  - [Merge Request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/{merge_id})\n" # MR Link
    summary += f"  - {humanized}\n" # Time ago
    summary += f"  - Notes: \n    - .\n" # static

print(summary)


current_date = arrow.now()
formatted_date = current_date.format('MMM DD')
created_issue = create_new_issue(project, issue_creation_token, formatted_date +" Modified Workflow Summary", summary)

issue_url = created_issue["web_url"]
print(f"\n\nCreated issue: {issue_url}")